package edu.ub.bioveg.fisioveg.sc.tools.macros;

/*
 * Cereal Scanner
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

/**
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 * Created on: Dec 4, 2016
 */
public abstract class MacroVars {
        
        protected String macroNameKey = null;
        protected String macroName = null;
        protected RSyntaxTextArea syntaxTextArea = null;
        
        public MacroVars() {
                
        }
        
        public final RSyntaxTextArea getSyntaxTextArea() {
                return syntaxTextArea;
        }

        public final void setSyntaxTextArea(RSyntaxTextArea syntaxTextArea) {
                this.syntaxTextArea = syntaxTextArea;
        }

        public final String getMacroNameKey() {
                return macroNameKey;
        }

        public final void setMacroNameKey(String macroNameKey) {
                this.macroNameKey = macroNameKey;
        }

        public final String getMacroName() {
                return macroName;
        }

        public final void setMacroName(String macroName) {
                this.macroName = macroName;
        }
        
        public final String getMacroCode() {
                return syntaxTextArea.getText();
        }
}
