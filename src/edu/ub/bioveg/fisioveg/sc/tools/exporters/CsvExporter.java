package edu.ub.bioveg.fisioveg.sc.tools.exporters;

/*
 * Cereal Scanner
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 *
 */
public final class CsvExporter {

        private CsvExporter() {

        }
        
        public final static void exportCsvResults(ResultsPojo resultsPojo, File saveResultsFile, String delimiter) {
                BufferedWriter bw = null;
                char delim = setDelimiter(delimiter);
                
                try {
                        bw = new BufferedWriter(new FileWriter(saveResultsFile, false));
                        writeHeadings(resultsPojo, delim, bw);

                        //-------------------
                        
                        List<ResultEntry> resultEntries = resultsPojo.getData();
                        for(int i = 0; i < resultEntries.size(); i++) {
                                ResultEntry re = resultEntries.get(i);
                                List<String> data = re.getData();
                                for(int j=0; j < data.size(); j++) {
                                        String datum = data.get(j);
                                        bw.write(datum);
                                        if(j < data.size()-1) {
                                                bw.write(delim);
                                        }
                                }
                                
                                bw.write(System.getProperty("line.separator"));
                                bw.flush();
                        }
                }
                catch(IOException ex) {
                        ex.printStackTrace();
                }
                finally {
                        if(bw != null) {
                                try {
                                        bw.close();
                                }
                                catch(IOException e) {}
                                bw = null;
                        }
                }
        }
        
        private static final void writeHeadings(ResultsPojo resultsPojo, char delim, BufferedWriter bw) throws IOException {
                List<String> headings = resultsPojo.getHeadings();
                for(int i=0; i < headings.size(); i++) {
                        String header = headings.get(i);
                        bw.write(header);
                        if(i < headings.size()-1) {
                                bw.write(delim);
                        }
                }
                
                bw.write(System.getProperty("line.separator"));
                bw.flush();
        }
        
        private static final char setDelimiter(String delimiter) {
                char delim = '\0';
                switch(delimiter) {
                        case "Comma": {
                                delim = ',';
                                break;
                        }
                        
                        case "Space": {
                                delim = ' ';
                                break;
                        }
                        
                        case "Tab": {
                                delim = '\t';
                                break;
                        }
                        
                        case "Pipe": {
                                delim = '|';
                                break;
                        }
                        
                        case "Semi-Colon": {
                                delim = ';';
                                break;
                        }
                        
                        default: {
                                delim = ',';
                                break;
                        }
                }
                
                return delim;
        }
}
