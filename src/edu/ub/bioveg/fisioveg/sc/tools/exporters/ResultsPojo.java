package edu.ub.bioveg.fisioveg.sc.tools.exporters;

/*
 * Cereal Scanner
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 *
 */
public class ResultsPojo {

        private ArrayList<String> headings = null;
        private ArrayList<ResultEntry> data = null;

        public ResultsPojo() {
                headings = new ArrayList<String>();
                data = new ArrayList<ResultEntry>();
        }

        public ResultsPojo(ArrayList<String> headings, ArrayList<ResultEntry> data) {
                this.headings = headings;
                this.data = data;
        }

        public List<String> getHeadings() {
                return headings;
        }

        public void setHeadings(ArrayList<String> headings) {
                this.headings = headings;
        }

        public List<ResultEntry> getData() {
                return data;
        }

        public void setData(ArrayList<ResultEntry> data) {
                this.data = data;
        }
        
        public void addResultEntry(ResultEntry re) {
                data.add(re);
        }
        
        public void addResultHeading(String heading) {
                headings.add(heading);
        }
}
