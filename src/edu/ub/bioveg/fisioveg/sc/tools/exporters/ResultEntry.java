package edu.ub.bioveg.fisioveg.sc.tools.exporters;

/*
 * Cereal Scanner
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 * 
 */
public class ResultEntry {

        private ArrayList<String> data = null;
        
        public ResultEntry() {
                data = new ArrayList<String>();
        }
        
        public final void addData(String datum) {
                data.add(datum);
        }
        
        public final void addData(int datum) {
                data.add(String.valueOf(datum));
        }
        
        public final void addData(double datum) {
                data.add(String.valueOf(datum));
        }
        
        public final List<String> getData() {
                return data;
        }
}
