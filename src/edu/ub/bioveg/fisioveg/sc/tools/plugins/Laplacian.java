package edu.ub.bioveg.fisioveg.sc.tools.plugins;

import ij.ImagePlus;
import ij.process.ByteProcessor;
import ij.process.ColorProcessor;
import ij.process.FHT;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

public class Laplacian {

        private int M, N, size, w, h;
        private ImagePlus imp;
        private FHT fht;
        private ImageProcessor mask;
        private ImageProcessor ipFilter;
        private ImageProcessor ip;

        public void setup(ImagePlus imp) {
                this.imp = imp;
                this.ip = imp.getProcessor();
                M = ip.getWidth();
                N = ip.getHeight();
        }

        // shows the power spectrum and filters the image
        public ImagePlus filtering() {
                int maxN = Math.max(M, N);
                size = 2;
                while(size < maxN) {
                        size *= 2;
                }

                FFT fft = new FFT();
                fft.run("forward", imp);

                h = Math.round((size - N) / 2);
                w = Math.round((size - M) / 2);
                // processor of the padded image
                ImageProcessor ip2 = ip.createProcessor(size, size);
                ip2.fill();
                ip2.insert(ip, w, h);

                if(ip instanceof ColorProcessor) {
                        ImageProcessor bright = ((ColorProcessor)ip2).getBrightness();
                        fht = new FHT(bright);
                        // get a duplication of brightness in order to add it after filtering
                        fht.rgb = (ColorProcessor)ip.duplicate();
                }
                else {
                        fht = new FHT(ip2);
                }

                fht.transform(); // calculates the Fourier transformation
                fht.originalColorModel = ip.getColorModel();
                fht.originalBitDepth = imp.getBitDepth();
                ipFilter = Lapl();
                fht.swapQuadrants(ipFilter);

                byte[] pixels_id = (byte[])ipFilter.getPixels();
                float[] pixels_fht = (float[])fht.getPixels();

                for(int i = 0; i < size * size; i++) {
                        pixels_fht[i] = (float)(pixels_fht[i] * (pixels_id[i] & 255) / 255.0);
                }

                mask = fht.getPowerSpectrum();
                ImagePlus imp2 = new ImagePlus("inverse FFT of " + imp.getTitle(), mask);
                imp2.setProperty("FHT", fht);
                imp2.setCalibration(imp.getCalibration());
                return doInverseTransform(fht);
        }

        // creates a Laplacian filter
        private ByteProcessor Lapl() {
                ByteProcessor proc = new ByteProcessor(M, N);
                double value = 0;
                int xcenter = (M / 2) + 1;
                int ycenter = (N / 2) + 1;

                for(int y = 0; y < N; y++) {
                        for(int x = 0; x < M; x++) {
                                value = (1 & 255) / 255 + Math.abs(x - xcenter) * Math.abs(x - xcenter) + Math.abs(y - ycenter) * Math.abs(y - ycenter);
                                proc.putPixelValue(x, y, value);
                        }
                }

                ByteProcessor ip2 = new ByteProcessor(size, size);
                byte[] p = (byte[])ip2.getPixels();
                for(int i = 0; i < size * size; i++) {
                        p[i] = (byte)255;
                }

                ip2.insert(proc, w, h);
                return ip2;
        }

        // applies the inverse Fourier transform to the filtered image
        private ImagePlus doInverseTransform(FHT fht) {
                fht = fht.getCopy();
                fht.inverseTransform();
                fht.resetMinAndMax();
                ImageProcessor ip2 = fht;
                fht.setRoi(w, h, M, N);
                ip2 = fht.crop();

                int bitDepth = fht.originalBitDepth > 0 ? fht.originalBitDepth : imp.getBitDepth();
                switch(bitDepth) {
                        case 8: {
                                ip2 = ip2.convertToByte(true);
                                break;
                        }

                        case 16: {
                                ip2 = ip2.convertToShort(true);
                                break;
                        }

                        case 24: {
                                if(fht.rgb == null || ip2 == null) {
                                        return null;
                                }

                                ColorProcessor rgb = (ColorProcessor)fht.rgb.duplicate();
                                rgb.setBrightness((FloatProcessor)ip2);
                                ip2 = rgb;
                                fht.rgb = null;
                                break;
                        }

                        case 32: {
                                break;
                        }
                }

                if(bitDepth != 24 && fht.originalColorModel != null) {
                        ip2.setColorModel(fht.originalColorModel);
                }

                String title = imp.getTitle();
                if(title.startsWith("FFT of ")) {
                        title = title.substring(7, title.length());
                }

                ImagePlus impInverseFFT = new ImagePlus("xxx Inverse FFT of " + title, ip2);
                if(impInverseFFT.getWidth() == imp.getWidth()) {
                        impInverseFFT.setCalibration(imp.getCalibration());
                }

                return impInverseFFT;
        }
}
