package edu.ub.bioveg.fisioveg.sc.tools.ui;

/*
 * Cereal Scanner
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.regex.Pattern;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 * Created on: Dec 16, 2015
 *
 */
public class RegexDocumentFilter extends DocumentFilter {

        private Pattern regex = null;
        private int maxLength = -1;
        private boolean isBlocked = false;

        public RegexDocumentFilter(String regex) {
                super();
                this.regex = Pattern.compile(regex);
        }

        public RegexDocumentFilter(int maxLength) {
                super();
                this.maxLength = maxLength;
                this.regex = Pattern.compile(".*");
        }

        public RegexDocumentFilter(String regex, int maxLength) {
                super();
                this.maxLength = maxLength;
                this.regex = Pattern.compile(regex);
        }

        public void setRegex(String regex) {
                this.regex = Pattern.compile(regex);
        }

        @Override
        public void insertString(FilterBypass fb, int offset, String str, AttributeSet attrs) throws BadLocationException {
                if(str == null) {
                        return;
                }

                if(maxLength < 0) {
                        if(regex.matcher(str).matches() || str.isEmpty()) {
                                if(!isBlocked) {
                                        fb.insertString(offset, str, attrs);
                                }
                                else {
                                        // send event
                                        // Max limit not reached but invalid input
                                }
                        }
                }
                else {
                        if(offset < maxLength && offset >= 0 && (regex.matcher(str).matches() || str.isEmpty()) && fb.getDocument().getLength() < maxLength) {
                                if(!isBlocked) {
                                        fb.insertString(offset, str, attrs);
                                }
                                else {
                                        // send event
                                        // Max limit reached
                                }
                        }
                }
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String str, AttributeSet attrs) throws BadLocationException {
                if(str == null) {
                        return;
                }

                if(maxLength < 0) {
                        if(regex.matcher(str).matches() || str.isEmpty()) {
                                if(!isBlocked) {
                                        fb.replace(offset, length, str, attrs);
                                }
                                else {
                                        // send event
                                        // Max limit not reached but invalid input
                                }
                        }
                }
                else {
                        if(offset < maxLength && offset >= 0 && (regex.matcher(str).matches() || str.isEmpty()) && fb.getDocument().getLength() < maxLength) {
                                if(!isBlocked) {
                                        fb.replace(offset, length, str, attrs);
                                }
                                else {
                                        // send event
                                        // Max limit reached
                                }
                        }
                }
        }
}
