package edu.ub.bioveg.fisioveg.sc;

/*
 * Cereal Scanner
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import javax.swing.UIManager;
import edu.ub.bioveg.fisioveg.sc.tools.ui.UITool;
import edu.ub.bioveg.fisioveg.sc.ui.ScannerFrame;
import ij.IJ;
import ij.plugin.PlugIn;

/**
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 * Created on: Nov 29, 2017
 */
public class CerealScanner implements PlugIn {
        
        public static final String VERSION = "2.12-beta";
        private static final String MIN_VERSION = "1.51n";
        private ScannerFrame frame = null;
        
        
        @Override
        public void run(String arg) {
                if (IJ.versionLessThan(MIN_VERSION)) {
                        IJ.showMessage("This plugin needs to run on ImageJ v"+MIN_VERSION+" and above");
                }
                else {
                        try {
                                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                        }
                        catch (Exception e) {
                                // Will use the standard Look&Feel
                        }
                        
                        frame = new ScannerFrame();
                        UITool.center(frame);
                        frame.setVisible(true);
                }
        }
        
        //For stand alone testing
        public static void main(String ...args) {
                new CerealScanner().run("");
        }
}
