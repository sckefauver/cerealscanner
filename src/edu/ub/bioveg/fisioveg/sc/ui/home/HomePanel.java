package edu.ub.bioveg.fisioveg.sc.ui.home;

/*
 * Cereal Scanner
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import edu.ub.bioveg.fisioveg.sc.tools.ui.UITool;

/**
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 * 
 */
public class HomePanel extends JPanel {

        private static final long serialVersionUID = 8222214036793371412L;

        public HomePanel() {
                setLayout(new BorderLayout(5, 5));
                setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
                setOpaque(true);
                setBackground(Color.WHITE);
                ImageIcon homeImage = UITool.getImageIcon("/edu/ub/bioveg/fisioveg/sc/ui/home/home_screen.png");
                JLabel label = new JLabel(homeImage);
                label.setOpaque(true);
                label.setBackground(Color.WHITE);
                add(label, BorderLayout.NORTH);
        }
}
