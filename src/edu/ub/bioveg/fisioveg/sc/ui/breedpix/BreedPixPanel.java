package edu.ub.bioveg.fisioveg.sc.ui.breedpix;

/*
 * Cereal Scanner
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import edu.ub.bioveg.fisioveg.sc.tools.breedpix.BreedPixResult;
import edu.ub.bioveg.fisioveg.sc.tools.breedpix.BreedPixTool;
import edu.ub.bioveg.fisioveg.sc.tools.breedpix.PicVIOperation;
import edu.ub.bioveg.fisioveg.sc.tools.breedpix.PixelMask;
import edu.ub.bioveg.fisioveg.sc.tools.ui.UITool;
import edu.ub.bioveg.fisioveg.sc.ui.ScannerFrame;
import ij.IJ;

/**
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 * Created on: Nov 29, 2017
 */
public class BreedPixPanel extends JPanel {

        private static final long serialVersionUID = 1749646560406230818L;
        
        private BreedPixOptionsPanel optionsPanel = null;
        private JPanel buttonPanel = null;
        private JButton processButton = null;
        private JProgressBar progressBar = null;
        
        public BreedPixPanel() {
                optionsPanel = new BreedPixOptionsPanel(true);
                
                processButton = new JButton("Process");
                processButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                processButton_actionPerformed();
                        }
                });
                
                progressBar = new JProgressBar(JProgressBar.HORIZONTAL);
                progressBar.setStringPainted(true);
                progressBar.setString("Ready");
                progressBar.setIndeterminate(false);
                
                buttonPanel = new JPanel(new BorderLayout(5, 5));
                buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                buttonPanel.add(processButton, BorderLayout.EAST);
                buttonPanel.add(progressBar, BorderLayout.CENTER);
                
                // ---------------------------------------------------
                
                ImageIcon placeHolder = UITool.getImageIcon("/edu/ub/bioveg/fisioveg/sc/ui/breedpix/placeholder_breedpix.png");
                JLabel label = new JLabel(placeHolder);
                
                // ---------------------------------------------------
                
                setLayout(new BorderLayout(5, 5));
                setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                add(optionsPanel, BorderLayout.NORTH);
                add(label, BorderLayout.CENTER);
                add(buttonPanel, BorderLayout.SOUTH);
        }
        
        private final void processButton_actionPerformed() {
                File batchInputDir = optionsPanel.getBtachInputDir();
                if(batchInputDir == null) {
                        JOptionPane.showMessageDialog(this, "Select a folder to read the input images", "Empty Batch Input", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                File saveResultsFile = optionsPanel.getSaveResultsFile();
                if(saveResultsFile == null) {
                        JOptionPane.showMessageDialog(this, "Select a results file name and location to store the results of the batch process", "Empty Results File", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                Thread breedPixThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                                EventQueue.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                                processButton.setEnabled(false);
                                                progressBar.setIndeterminate(true);
                                                progressBar.setString("Running BreedPix ...");
                                                ScannerFrame.setBreedPixRunning(true);
                                        }
                                });
                                
                                process();
                                System.gc();
                                
                                EventQueue.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                                progressBar.setIndeterminate(false);
                                                progressBar.setString("Ready");
                                                processButton.setEnabled(true);
                                                ScannerFrame.setBreedPixRunning(false);
                                        }
                                });
                        }
                });
                
                breedPixThread.start();
        }
        
        private final void process() {
                File batchInputDir = optionsPanel.getBtachInputDir();
                File[] imageFiles = batchInputDir.listFiles();
                PicVIOperation picViOperation = new PicVIOperation();
                File imageFile = null;
                String ext = null;
                String fileName = null;
                String imagePath = null;
                
                File saveImageDir = optionsPanel.getSaveImageDir();
                if(saveImageDir != null) {
                        imagePath = saveImageDir.getAbsolutePath();
                }
                
                String delimiter = optionsPanel.getCsvDelimiter();
                char delim = '\0';
                switch(delimiter) {
                        case "Comma": {
                                delim = ',';
                                break;
                        }
                        
                        case "Space": {
                                delim = ' ';
                                break;
                        }
                        
                        case "Tab": {
                                delim = '\t';
                                break;
                        }
                        
                        case "Pipe": {
                                delim = '|';
                                break;
                        }
                        
                        case "Semi-Colon": {
                                delim = ';';
                                break;
                        }
                        
                        default: {
                                delim = ',';
                                break;
                        }
                }
                
                StringBuilder csvBuilder = new StringBuilder();
                csvBuilder.append("Image Name");
                csvBuilder.append(delim);
                csvBuilder.append("Intensity");
                csvBuilder.append(delim);
                csvBuilder.append("Hue");
                csvBuilder.append(delim);
                csvBuilder.append("Saturation");
                csvBuilder.append(delim);
                csvBuilder.append("Lightness");
                csvBuilder.append(delim);
                csvBuilder.append("a*");
                csvBuilder.append(delim);
                csvBuilder.append("b*");
                csvBuilder.append(delim);
                csvBuilder.append("u*");
                csvBuilder.append(delim);
                csvBuilder.append("v*");
                csvBuilder.append(delim);
                csvBuilder.append("GA");
                csvBuilder.append(delim);
                csvBuilder.append("GGA");
                csvBuilder.append(delim);
                csvBuilder.append("CSI");
                csvBuilder.append(System.getProperty("line.separator"));
                
                for(int i=0; i < imageFiles.length; i++) {
                        imageFile = imageFiles[i];
                        fileName = imageFile.getName();
                        ext = fileName.substring(fileName.lastIndexOf('.')+1).toLowerCase();
                        
                        switch(ext) {
                                case "jpg":
                                case "jpeg":
                                case "png":
                                case "tif":
                                case "tiff": {
                                        fileName = fileName.substring(0, fileName.lastIndexOf('.'));
                                        
                                        BufferedImage image = openImage(imageFile);
                                        BufferedImage scaledRenderedImage = BreedPixTool.reduceToMaxSize(image, 1024*768);
                                        BreedPixResult result = picViOperation.execute(scaledRenderedImage);
                                        
                                        if( result != null) {
                                                if(optionsPanel.isSaveGaImage()) {
                                                        PixelMask gaRoi = result.getGa_roi();
                                                        BufferedImage gaRoiImage = BreedPixTool.paintBWNotROI(scaledRenderedImage, gaRoi, fileName);
                                                        
                                                        try {
                                                                ImageIO.write(gaRoiImage, "jpg", new File(imagePath+File.separator+fileName+"_GA.JPG"));
                                                        }
                                                        catch(IOException ioe) {
                                                                IJ.log("Error saving GA ROI image: " + fileName);
                                                                IJ.log("Error: "+ioe.getMessage());
                                                        }
                                                        finally {
                                                                gaRoi = null;
                                                                gaRoiImage = null;
                                                        }
                                                }
                                                
                                                if(optionsPanel.isSaveGgaImage()) {
                                                        PixelMask ggaRoi = result.getGga_roi();
                                                        BufferedImage ggaRoiImage = BreedPixTool.paintBWNotROI(scaledRenderedImage, ggaRoi, fileName);
                                                        
                                                        try {
                                                                ImageIO.write(ggaRoiImage, "jpg", new File(imagePath+File.separator+fileName+"_GGA.JPG"));
                                                        }
                                                        catch(IOException ioe) {
                                                                IJ.log("Error saving GGA ROI image: " + fileName);
                                                                IJ.log("Error: "+ioe.getMessage());
                                                        }
                                                        finally {
                                                                ggaRoi = null;
                                                                ggaRoiImage = null;
                                                        }
                                                }
                                                
                                                csvBuilder.append(fileName).append('.').append(ext);
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getIhs_i());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getIhs_h());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getIhs_s());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getLab_l());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getLab_a());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getLab_b());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getLuv_u());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getLuv_v());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getGa());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getGga());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getCsi());
                                                csvBuilder.append(System.getProperty("line.separator"));
                                        }
                                        
                                        scaledRenderedImage = null;
                                        image = null;
                                        fileName = null;
                                        imageFile = null;
                                        break;
                                }
                                
                                default: {
                                        break;
                                }
                        }
                }
                
                printResults(csvBuilder.toString());
        }
        
        private final void printResults(String results) {
                File saveResultsFile = optionsPanel.getSaveResultsFile();
                try {
                        FileWriter fw = new FileWriter(saveResultsFile);
                        fw.write(results);
                        fw.flush();
                        fw.close();
                        fw = null;
                }
                catch(IOException ioe) {
                        IJ.log("Error saving csv file: " + saveResultsFile.getAbsoluteFile());
                        IJ.log("Error: " + ioe.getMessage());
                        IJ.error("I/O Error", "There was an error while saving the csv file.");
                }
        }

        private final BufferedImage openImage(File imageFile) {
                BufferedImage image = null;
                try {
                        image = IJ.openImage(imageFile.getAbsolutePath()).getBufferedImage();
                }
                catch(Exception ex) {
                        IJ.log("Error opening image: " + imageFile.getName());
                        IJ.log("Error: " + ex.getMessage());
                }

                return image;
        }
}
