package edu.ub.bioveg.fisioveg.sc.ui.earlyvigor;

/*
 * Cereal Scanner
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import edu.ub.bioveg.fisioveg.sc.tools.exporters.CsvExporter;
import edu.ub.bioveg.fisioveg.sc.tools.exporters.ResultEntry;
import edu.ub.bioveg.fisioveg.sc.tools.exporters.ResultsPojo;
import edu.ub.bioveg.fisioveg.sc.tools.ui.FileOpen;
import edu.ub.bioveg.fisioveg.sc.tools.ui.FileSave;
import edu.ub.bioveg.fisioveg.sc.tools.ui.UITool;
import layout.TableLayout;

/**
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 *
 */
public class EarlyVigorAreaPanel extends JPanel {
        
        private static final long serialVersionUID = 7927287066850028055L;
        
        private JLabel batchInputLabel = null;
        private JTextField batchInputField = null;
        private JButton batchInputButton = null;
        
        private JLabel resultsFileLabel = null;
        private JTextField resultsFileField = null;
        private JButton resultsFileButton = null;
        
        private JPanel optionsPanel = null;
        
        private JPanel buttonPanel = null;
        private JButton processButton = null;
        private JProgressBar progressBar = null;
        
        private String recentDir = null;
        private File batchInputDir = null;
        private File saveResultsFile = null;
        
        public EarlyVigorAreaPanel() {
                batchInputLabel = new JLabel("Batch Inputs:");
                batchInputLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                batchInputField = new JTextField(20);
                batchInputField.setEditable(false);
                batchInputField.setBackground(Color.WHITE);
                
                batchInputButton = new JButton("...");
                batchInputButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                batchInputButton_actionPerformed();
                        }
                });
                
                //----------------------------------------------------------------
                
                resultsFileLabel = new JLabel("Results File:");
                resultsFileLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                resultsFileField = new JTextField(20);
                resultsFileField.setEditable(false);
                resultsFileField.setBackground(Color.WHITE);
                
                resultsFileButton = new JButton("...");
                resultsFileButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                resultsFileButton_actionPerformed();  
                        }
                });
                
                //----------------------------------------------------------------
                
                double spacer = 5;
                double[][] layoutSize = {
                                //                   0,                        2,                             4
                                {TableLayout.PREFERRED, spacer, TableLayout.FILL, spacer, TableLayout.PREFERRED},
                                {TableLayout.PREFERRED, //0
                                 spacer,
                                 TableLayout.PREFERRED  //2
                                }
                };
                
                optionsPanel = new JPanel();
                optionsPanel.setBorder(BorderFactory.createTitledBorder("Options"));
                optionsPanel.setLayout(new TableLayout(layoutSize));
                optionsPanel.add(batchInputLabel,           "0, 0");
                optionsPanel.add(batchInputField,           "2, 0");
                optionsPanel.add(batchInputButton,          "4, 0");
                optionsPanel.add(resultsFileLabel,          "0, 2");
                optionsPanel.add(resultsFileField,          "2, 2");
                optionsPanel.add(resultsFileButton,         "4, 2");
                
                //----------------------------------------------------------------
                
                processButton = new JButton("Process");
                processButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                processButton_actionPerformed();
                        }
                });
                
                progressBar = new JProgressBar(JProgressBar.HORIZONTAL);
                progressBar.setStringPainted(true);
                progressBar.setString("Ready");
                progressBar.setIndeterminate(false);
                
                buttonPanel = new JPanel(new BorderLayout(5, 5));
                buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                buttonPanel.add(processButton, BorderLayout.EAST);
                buttonPanel.add(progressBar, BorderLayout.CENTER);
                
                // ---------------------------------------------------
                
                ImageIcon placeHolder = UITool.getImageIcon("/edu/ub/bioveg/fisioveg/sc/ui/earlyvigor/placeholder_early_vigor.png");
                JLabel label = new JLabel(placeHolder);
                
                //----------------------------------------------------
                
                setLayout(new BorderLayout(5, 5));
                setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                add(optionsPanel, BorderLayout.NORTH);
                add(label, BorderLayout.CENTER);
                add(buttonPanel, BorderLayout.SOUTH);
        }
        
        private final void processButton_actionPerformed() {
                if(batchInputDir == null) {
                        JOptionPane.showMessageDialog(this, "Please select the batch inputs folder.", "Select Batch Inputs", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                if(saveResultsFile == null) {
                        JOptionPane.showMessageDialog(this, "Please select a results file location.", "Select Results File", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                                processButton.setEnabled(false);
                                progressBar.setIndeterminate(true);
                                progressBar.setString("Run Early Vigor Area Estimation ...");
                                
                                File[] imageFiles = batchInputDir.listFiles();
                                ResultsPojo resultsPojo = new ResultsPojo();
                                resultsPojo.addResultHeading("filename");
                                resultsPojo.addResultHeading("area");
                                
                                for(int i = 0; i < imageFiles.length; i++) {
                                        String fileName = imageFiles[i].getName().toLowerCase();
                                        if(fileName.endsWith(".jpg") || fileName.endsWith("jpeg")) {
                                                String area = EarlyVigorAreaEstimator.estimateArea(imageFiles[i]);
                                                ResultEntry re = new ResultEntry();
                                                re.addData(fileName);
                                                re.addData(area);
                                                resultsPojo.addResultEntry(re);
                                        }
                                }
                                
                                EventQueue.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                                progressBar.setString("Exporting to file");
                                        }
                                });
                                
                                CsvExporter.exportCsvResults(resultsPojo, saveResultsFile, "Comma");
                                
                                EventQueue.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                                progressBar.setIndeterminate(false);
                                                progressBar.setString("Ready");
                                                processButton.setEnabled(true);
                                        }
                                });
                        }
                });
                
                thread.start();
        }
        
        private final void batchInputButton_actionPerformed() {
                batchInputDir = FileOpen.getFile("Select batch input folder", (recentDir == null ? System.getProperty("user.dir") : recentDir), JFileChooser.DIRECTORIES_ONLY, "Batch Image Folder", (String[]) null);
                if (batchInputDir != null) {
                        recentDir = batchInputDir.getAbsolutePath();
                        batchInputField.setText(batchInputDir.getAbsolutePath());
                }
        }
        
        private final void resultsFileButton_actionPerformed() {
                saveResultsFile = FileSave.saveFile("Name Results File", (recentDir == null ? new File(System.getProperty("user.dir")) : new File(recentDir)), "Results File", "Early_Vigor_Area_Estimation_Results.csv");
                if(saveResultsFile != null) {
                        recentDir = saveResultsFile.getParentFile().getAbsolutePath();
                        resultsFileField.setText(saveResultsFile.getAbsolutePath());
                }
        }
}
