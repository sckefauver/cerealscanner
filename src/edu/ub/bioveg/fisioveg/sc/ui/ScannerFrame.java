package edu.ub.bioveg.fisioveg.sc.ui;

/*
 * Cereal Scanner
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import edu.ub.bioveg.fisioveg.sc.CerealScanner;
import edu.ub.bioveg.fisioveg.sc.ui.breedpix.BreedPixPanel;
import edu.ub.bioveg.fisioveg.sc.ui.earlyvigor.EarlyVigorAreaPanel;
import edu.ub.bioveg.fisioveg.sc.ui.home.HomePanel;
import edu.ub.bioveg.fisioveg.sc.ui.maturity.MaturityPanel;
import edu.ub.bioveg.fisioveg.sc.ui.ngrdi.tgi.NgrdiTgiMacroPanel;
import edu.ub.bioveg.fisioveg.sc.ui.wec.WheatEarCountingPanel;

/**
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 * Created on: Nov 29, 2017
 */
public class ScannerFrame extends JFrame {
        
        private static final long serialVersionUID = -7308181228943639611L;
        
        private JTabbedPane tabbedPane = null;
        private JTabbedPane cerealScannerTabbedPane = null;
        private JTabbedPane biosmassTabbedPane = null;
        private BreedPixPanel breedPixPanel = null;
        private NgrdiTgiMacroPanel ngrdiTgiMacroPanel = null;
        private WheatEarCountingPanel wecPanel = null;
        private EarlyVigorAreaPanel photoSynthAreaPanel = null;
        private MaturityPanel maturityPanel = null;
        private HomePanel homePanel = null;
        
        private static boolean breedPixRunning = false;
        private static boolean macrosRunning = false;
        
        public ScannerFrame() {
                super();
                init();
        }
        
        private final void init() {
                breedPixPanel = new BreedPixPanel();
                ngrdiTgiMacroPanel = new NgrdiTgiMacroPanel();
                wecPanel = new WheatEarCountingPanel();
                photoSynthAreaPanel = new EarlyVigorAreaPanel();
                maturityPanel = new MaturityPanel();
                homePanel = new HomePanel();
                
                cerealScannerTabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);
                cerealScannerTabbedPane.addTab("EarlyVigor", photoSynthAreaPanel);
                cerealScannerTabbedPane.addTab("Ear Counting", wecPanel);
                cerealScannerTabbedPane.addTab("Maturity", maturityPanel);
                
                biosmassTabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);
                biosmassTabbedPane.addTab("Breedpix", breedPixPanel);
                biosmassTabbedPane.addTab("NGRDI/TGI", ngrdiTgiMacroPanel);
                
                tabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);
                tabbedPane.addTab("Home", homePanel);
                tabbedPane.addTab("Cereal Scanner", cerealScannerTabbedPane);
                tabbedPane.addTab("Biomass", biosmassTabbedPane);

                // ---------------------------------------------------

                setTitle("Cereal Scanner - " + CerealScanner.VERSION);
                setLayout(new BorderLayout(5, 5));
                add(tabbedPane, BorderLayout.CENTER);

                setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent e) {
                                exit();
                        }
                });
                
                pack();
        }
        
        public static final void setBreedPixRunning(boolean running) {
                breedPixRunning = running;
        }
        
        public static final void setMacroRunning(boolean running) {
                macrosRunning = running;
        }
        
        public static final boolean isMacroRunning() {
                return macrosRunning;
        }
        
        public static final boolean isBreedPixRunning() {
                return breedPixRunning;
        }
        
        private final void exit() {
                
                //TODO code this
//                StringBuilder sb = new StringBuilder();
//                
//                if(scannerRunning || macrosRunning) {
//                        sb.append("There are still tasks running such as");
//                        if(scannerRunning) {
//                                sb.append(" \"Scanner\"");
//                        }
//                        
//                        if(macrosRunning) {
//                                if(scannerRunning) {
//                                        sb.append(" and");
//                                }
//                                
//                                sb.append(" \"Canopy Macros\"");
//                        }
//                        
//                        if(breedPixRunning) {
//                                if(macrosRunning || scannerRunning) {
//                                        sb.append(" and");
//                                }
//                                
//                                sb.append(" \"BreedPix\"");
//                        }
//                        
//                        sb.append(". Exiting now may leave results unfinished, still exit ?");
//                }
//                else {
//                        sb.append("Exit the plugin ?");
//                }
//                
//                String msg = sb.toString();
//                
//                int choice = JOptionPane.showConfirmDialog(this, msg, "Exit Plugin ?", JOptionPane.YES_NO_CANCEL_OPTION);
//                if(JOptionPane.YES_OPTION == choice) {
//                        setVisible(false);
//                        dispose();
//                        System.gc();
//                }
                
                
                String msg = null;
                if(breedPixRunning || macrosRunning) {
                        //TODO fix this up 
                        msg = "There are still tasks running such as \"BreedPix\" or \"Other Macro\" Exiting now may leave results unfinished, still exit?";
                }
                else {
                        msg = "Exit the plugin?";
                }

                int choice = JOptionPane.showConfirmDialog(this, msg, "Exit Plugin ?", JOptionPane.YES_NO_CANCEL_OPTION);
                if(JOptionPane.YES_OPTION == choice) {
                        setVisible(false);
                        dispose();
                        System.gc();
                }
        }
}
